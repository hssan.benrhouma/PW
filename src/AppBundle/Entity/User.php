<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;
use  AppBundle\Entity\Shop;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
  /**
   * @ORM\OneToOne(targetEntity="Shop", cascade={"persist"})
   */
    public $shop;
    public function __construct()
    {
        parent::__construct();
        $this->shops = new \Doctrine\Common\Collections\ArrayCollection();
        // your own logic
    }

    /**
     * Add shop
     *
     * @param \AppBundle\Entity\Shop $shop
     *
     * @return User
     */
    public function addShop(\AppBundle\Entity\Shop $shop)
    {
        $shop->setUser($this);
        $this->shops[] = $shop;

        return $this;
    }

    /**
     * Remove shop
     *
     * @param \AppBundle\Entity\Shop $shop
     */
    public function removeShop(\AppBundle\Entity\Shop $shop)
    {
        $this->shops->removeElement($shop);
    }

    /**
     * Get shops
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShops()
    {
        return $this->shops;
    }

    /**
     * Set shop
     *
     * @param \AppBundle\Entity\Shop $shop
     *
     * @return User
     */
    public function setShop(\AppBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \AppBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }
}
