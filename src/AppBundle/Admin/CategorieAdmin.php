<?php
// src/AppBundle/Admin/CategorieAdmin.php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CategorieAdmin extends AbstractAdmin
{
    protected $baseRouteName = '';
    protected $baseRoutePattern = '';
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')





    ;
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')

        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)

    {



        $listMapper

            ->add('name')

            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                )
            ))

        ;
    }

    // Fields to be shown on show action
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')


        ;
    }

}