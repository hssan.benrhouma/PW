<?php
// src/AppBundle/Admin/CategorieAdmin.php
namespace AppBundle\Admin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ShopAdmin extends AbstractAdmin
{
    protected $baseRouteName = '';
    protected $baseRoutePattern = '';
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('telephone')

            ->add('adresse')
->add('longitude')
->add('latitude')
   


    ;
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
          ->add('name')
            ->add('telephone')

            ->add('adresse')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)

    {



        $listMapper

     ->add('name')
            ->add('telephone')
            ->add('adresse')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                )
            ))

        ;
    }

    // Fields to be shown on show action
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
  ->add('name')
            ->add('telephone')

            ->add('adresse')
        ;
    }

/**
     * {@inheritdoc}
     */
    public function prePersist($object)
    {
$user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
if($user->hasRole('ROLE_ADMIN_SHOP'))
{
        $object->setUser($user);

}
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($object)
    {
$user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
if($user->hasRole('ROLE_ADMIN_SHOP'))
{
        $object->setUser($user);

}
    }

           public function createQuery($context = 'list')
    {

$user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
if($user->hasRole('ROLE_ADMIN_SHOP'))
        {
          $query = parent::createQuery($context);

       
        $alias = $query->getRootAlias();

        $query->leftJoin($alias.'.user', 'user')
            ->andWhere($query->expr()->in('user.id', ':my_param'));
        $query->setParameter('my_param',$this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser()->getId() );
        return $query;  
        }
        else{
            
       $query = parent::createQuery($context);

       
        return $query;
    
        }
    
    }
}