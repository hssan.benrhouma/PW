<?php
// src/AppBundle/Admin/CategorieAdmin.php
namespace AppBundle\Admin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends AbstractAdmin
{
    protected $baseRouteName = '';
    protected $baseRoutePattern = '';
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('email')
 ->add('password')
  ->add('shop')
   


    ;
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
          ->add('username')
            ->add('email')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)

    {



        $listMapper

     ->add('username')
            ->add('email')

            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                )
            ))

        ;
    }

    // Fields to be shown on show action
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
->add('username')
            ->add('email')

        ;
    }



   



    /**
     * {@inheritdoc}
     */
    public function prePersist($object)
    {

        $object->addRole('ROLE_ADMIN_SHOP');
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($object)
    {
        $object->addRole('ROLE_ADMIN_SHOP');

    }
}