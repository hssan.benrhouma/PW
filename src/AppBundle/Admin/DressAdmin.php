<?php
// src/AppBundle/Admin/CategorieAdmin.php
namespace AppBundle\Admin;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DressAdmin extends AbstractAdmin
{
    protected $baseRouteName = '';
    protected $baseRoutePattern = '';
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                    ->add('categorie')

           ->add('ref')
            ->add('name')
            ->add('shop');
 if ($this->id($this->getSubject())) {
    // EDIT
     $formMapper
            ->add('img', FileType::class, array('label' => 'Image', 'data_class' => null))
  ;
  }
  else {
    // CREATE
 $formMapper
        ->add('img', FileType::class, array('label' => 'Image'))
;
  }



    
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('ref')
            ->add('name')
            ->add('shop')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)

    {



        $listMapper

        ->add('ref')
            ->add('name')
            ->add('shop')

            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                )
            ))

        ;
    }

    // Fields to be shown on show action
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('ref')
            ->add('name')
            ->add('shop')
              ->add('img', NULL, array(
                'template' => 'admin/default/picture.html.twig'
            ));
    }

         public function createQuery($context = 'list')
    {

$user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
if($user->hasRole('ROLE_ADMIN_SHOP'))
        {
          $query = parent::createQuery($context);

       
        $alias = $query->getRootAlias();

         $query->leftJoin($alias.'.shop', 's')
            ->leftJoin('s.user', 'user')
            ->andWhere($query->expr()->in('user.id', ':my_param'));
        $query->setParameter('my_param',$user->getId() );
        return $query;  
        }
        else{
            
       $query = parent::createQuery($context);

       
        return $query;
    
        }
    
    }

     public function prePersist($page)
    {
        $this->manageEmbeddedImageAdmins($page);
    }

    public function preUpdate($page)
    {
        $this->manageEmbeddedImageAdmins($page);
    }

    private function manageEmbeddedImageAdmins($page)
    {
        // Cycle through each field
        foreach ($this->getFormFieldDescriptions() as $fieldName => $fieldDescription) {
            // detect embedded Admins that manage Images
            if ($fieldDescription->getType() === 'sonata_type_admin' &&
                ($associationMapping = $fieldDescription->getAssociationMapping()) &&
                $associationMapping['targetEntity'] === 'AppBundle\Entity\Image'
            ) {
                $getter = 'get'.$fieldName;
                $setter = 'set'.$fieldName;

                /** @var Image $image */
                $image = $page->$getter();

                if ($image) {
                    if ($image->getFile()) {
                        // update the Image to trigger file management
                        $image->refreshUpdated();
                    } elseif (!$image->getFile() && !$image->getFilename()) {
                        // prevent Sf/Sonata trying to create and persist an empty Image
                        $page->$setter(null);
                    }
                }
            }
        }
    }
}